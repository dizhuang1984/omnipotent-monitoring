package com.empire.monitoring.dao;

import org.springframework.data.repository.CrudRepository;

import com.empire.monitoring.model.Users;


/**
 * CrudRepository接口
 *
 *
 */
public interface UsersRepositoryCrudRepository extends CrudRepository<Users, Integer> {

}
