package com.empire.monitoring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empire.monitoring.model.AppHostMonitor;

/**
 * 类AppHostMonitorRepository.java的实现描述：应用主机监控实体dao操作
 * 
 * @author arron 2018年5月21日 下午11:22:18
 */
public interface AppHostMonitorRepository extends JpaRepository<AppHostMonitor, Integer> {

}
