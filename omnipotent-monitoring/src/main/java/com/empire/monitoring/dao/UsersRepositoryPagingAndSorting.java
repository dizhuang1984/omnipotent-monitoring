package com.empire.monitoring.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.empire.monitoring.model.Users;

/**
 * 
 *PagingAndSortingRepository接口
 *
 */
public interface UsersRepositoryPagingAndSorting extends PagingAndSortingRepository<Users,Integer> {

}
