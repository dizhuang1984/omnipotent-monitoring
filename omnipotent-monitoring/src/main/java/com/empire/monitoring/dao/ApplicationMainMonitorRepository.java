package com.empire.monitoring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empire.monitoring.model.ApplicationMainMonitor;

/**
 * 类ApplicationMainMonitorRepository.java的实现描述：主应用监控dao操作
 * 
 * @author arron 2018年5月21日 下午11:23:44
 */
public interface ApplicationMainMonitorRepository extends JpaRepository<ApplicationMainMonitor, Integer> {

}
