package com.empire.monitoring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empire.monitoring.model.Roles;

/**
 * RolesRepository
 *
 *
 */
public interface RolesRepository extends JpaRepository<Roles,Integer> {

}
