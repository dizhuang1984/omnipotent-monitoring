package com.empire.monitoring.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

//@Controller
@RestController
//controller的所有方法返回值会做json转换
public class FileUploadController {
    @RequestMapping(value = "/uploadController", method = RequestMethod.POST)
    public Map<String, String> fileUpload(MultipartFile filename) throws Exception, IOException {
        System.out.println(filename.getOriginalFilename());
        filename.transferTo(new File("e:/" + filename.getOriginalFilename()));
        Map<String, String> maps = new HashMap<>();
        maps.put("code", "success");
        return maps;
    }
}
