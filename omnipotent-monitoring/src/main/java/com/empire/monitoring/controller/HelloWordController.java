package com.empire.monitoring.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Administrator
 */
@Controller
public class HelloWordController {
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getHelloWord() {
        Map<String, String> map = new HashMap<>();
        System.out.println("222");
        map.put("hello", "word");
        return map;
    }

    @RequestMapping(value = "/hello2", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getHelloWord2() {
        Map<String, String> map = new HashMap<>();
        String bb = null;
        bb.length();
        map.put("hello", "word");
        return map;
    }

    @RequestMapping(value = "/hello3", method = RequestMethod.GET)
    public String showInfo2() {
        int a = 10 / 0;
        return "index";
    }
    //	@ExceptionHandler(value={java.lang.ArithmeticException.class})
    //	public ModelAndView  arithmeticExceptionHandler(Exception e){
    //		ModelAndView mv=new ModelAndView();
    //		mv.addObject("error", e.toString());
    //		mv.setViewName("error1");
    //		return mv;
    //	}
}
