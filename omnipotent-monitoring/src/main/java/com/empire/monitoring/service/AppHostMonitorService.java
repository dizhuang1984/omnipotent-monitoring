package com.empire.monitoring.service;

import com.empire.monitoring.model.AppHostMonitor;

/**
 * 类AppHostMonitorService.java的实现描述：AppHostMonitorService接口类
 * 
 * @author arron 2018年5月21日 下午11:25:40
 */
public interface AppHostMonitorService {
    void save(AppHostMonitor appHostMonitor);
}
