package com.empire.monitoring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.empire.monitoring.dao.AppHostMonitorRepository;
import com.empire.monitoring.model.AppHostMonitor;

@Service
public class AppHostMonitorServiceImpl implements AppHostMonitorService {
    @Autowired
    private AppHostMonitorRepository appHostMonitorRepository;

    @Transactional
    @Override
    public void save(AppHostMonitor appHostMonitor) {
        this.appHostMonitorRepository.save(appHostMonitor);
    }
}
