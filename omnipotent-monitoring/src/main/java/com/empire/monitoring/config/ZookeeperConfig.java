package com.empire.monitoring.config;

import java.io.IOException;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.context.annotation.Bean;

/**
 * 类ZookeeperConfig.java的实现描述：zookeeper配置类
 * 
 * @author arron 2018年5月30日 上午10:41:09
 */
//@Configuration
public class ZookeeperConfig {
    @Bean
    public ZooKeeper getZooKeeperConnect(Watcher watcher) {
        ZooKeeper zookeeper = null;
        try {
            //,slave02:2181,slave03:2181
            zookeeper = new ZooKeeper("127.0.0.1:2181", 5000, watcher);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return zookeeper;
    }

    @Bean
    public Watcher getWatcher() {
        return new Watcher() {
            // 监控所有被触发的事件
            public void process(WatchedEvent event) {
                //dosomething
            }
        };
    }
}
