package com.empire.monitoring.enums;

/**
 * 类AppMonitorRegisterTypeEnum.java的实现描述：AppHostMonitor.appMonitorRegisterType
 * 应用注册类型1被动(手动在监控中心添加),2.主动(app应用主动注册)；
 * 
 * @author arron 2018年5月21日 下午6:48:05
 */
public enum AppMonitorRegisterTypeEnum {
    /**
     * 1被动(手动在监控中心添加)
     */
    ACTIVE("主动", 1),
    /**
     * 2.主动(app应用主动注册)
     */
    PASSIVE("被动", 2);
    /**
     * 应用注册类型名称
     */
    private String  name;
    /**
     * 应用注册类型code值
     */
    private Integer value;

    /**
     * 根据枚举值获得枚举对象
     * 
     * @param id
     * @return
     */
    public static AppMonitorRegisterTypeEnum getAppMonitorRegisterTypeEnumByValue(Integer value) {
        for (AppMonitorRegisterTypeEnum item : AppMonitorRegisterTypeEnum.values()) {
            if (item.getValue().equals(value))
                return item;
        }
        return null;
    }

    private AppMonitorRegisterTypeEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
