package com.empire.monitoring.enums;

/**
 * 类AppMonitorTypeEnum.java的实现描述：AppHostMonitor.appMonitorType字段,应用注册的监控类型
 * 应用注册的监控类型:1.javamelody，2.druid，3.other；
 * 
 * @author arron 2018年5月21日 下午6:17:48
 */
public enum AppMonitorTypeEnum {
    /**
     * javamelody
     */
    JAVAMELODY("javamelody", 1),
    /**
     * druid
     */
    DRUID("druid", 2),
    /**
     * other
     */
    OTHER("other", 3);
    /**
     * 应用注册的监控类型名称
     */
    private String  name;
    /**
     * 应用注册的监控类型code值
     */
    private Integer value;

    /**
     * 根据枚举值获得枚举对象
     * 
     * @param id
     * @return
     */
    public static AppMonitorTypeEnum getAppMonitorTypeEnumByValue(Integer value) {
        for (AppMonitorTypeEnum item : AppMonitorTypeEnum.values()) {
            if (item.getValue().equals(value))
                return item;
        }
        return null;
    }

    private AppMonitorTypeEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

}
