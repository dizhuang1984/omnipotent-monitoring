package com.empire.monitoring.enums;

/**
 * 类ApplicationWarningTypeEnum.java的实现描述：
 * ApplicationMainMonitor.applicationWarningType字段,应用预警类型:1.邮箱，2.微信，3.短信， ；
 * 
 * @author arron 2018年5月21日 下午11:06:27
 */
public enum ApplicationWarningTypeEnum {
    /**
     * 1.邮箱3.短信
     */
    EMAIL("邮箱", 1),
    /**
     * 2.微信
     */
    OFFLINE("微信", 2),
    /**
     * 3.短信
     */
    SMS("短信", 3);
    /**
     * 应用预警类型名称
     */
    private String  name;
    /**
     * 应用预警类型code值
     */
    private Integer value;

    /**
     * 根据枚举值获得枚举对象
     * 
     * @param id
     * @return
     */
    public static ApplicationWarningTypeEnum getApplicationWarningTypeEnumByValue(Integer value) {
        for (ApplicationWarningTypeEnum item : ApplicationWarningTypeEnum.values()) {
            if (item.getValue().equals(value))
                return item;
        }
        return null;
    }

    private ApplicationWarningTypeEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
