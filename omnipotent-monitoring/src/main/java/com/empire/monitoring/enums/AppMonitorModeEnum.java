package com.empire.monitoring.enums;

/**
 * 类AppMonitorModeEnum.java的实现描述：AppHostMonitor.appMonitorMode字段,监测方式
 * 0.不监控,1.niosocket.心跳包上报方式，2.url回调，3.主动http上报，4.mq方式，5.webservice， 6.other
 * 
 * @author arron 2018年5月21日 下午6:25:33
 */
public enum AppMonitorModeEnum {
    /**
     * 不监控
     */
    NULL(0, "不监控"),
    /**
     * niosocket.心跳包上报方式
     */
    NIOSOCKET(1, "niosocket心跳包"),
    /**
     * url回调
     */
    CALLBACK(2, "url回调"),
    /**
     * 主动http上报
     */
    ACTIVEHTTP(3, "主动http上报"),
    /**
     * mq方式
     */
    MQ(4, "mq方式"),

    WEBSERVICE(5, "webservice"),
    /**
     * other
     */
    OTHER(6, "other");
    /**
     * 监测方式说明
     */
    private String  desc;
    /**
     * 监测方式值
     */
    private Integer value;

    /**
     * 根据枚举值获得枚举对象
     * 
     * @param id
     * @return
     */
    public static AppMonitorModeEnum getAppMonitorModeEnumByValue(Integer value) {
        for (AppMonitorModeEnum item : AppMonitorModeEnum.values()) {
            if (item.getValue().equals(value))
                return item;
        }
        return null;
    }

    private AppMonitorModeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getValue() {
        return value;
    }

}
