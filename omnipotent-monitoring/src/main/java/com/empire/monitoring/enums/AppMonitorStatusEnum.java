package com.empire.monitoring.enums;

/**
 * 类AppMonitorStatusEnum.java的实现描述：AppHostMonitor.status字段,当前状态:1.在线，2.离线
 * 
 * @author arron 2018年5月21日 下午10:41:09
 */
public enum AppMonitorStatusEnum {
    /**
     * online.在线
     */
    ONLINE("online", 1),
    /**
     * offline.离线
     */
    OFFLINE("offline", 2);
    /**
     * 应用状态名称
     */
    private String  name;
    /**
     * 应用状态code值
     */
    private Integer value;

    /**
     * 根据枚举值获得枚举对象
     * 
     * @param id
     * @return
     */
    public static AppMonitorStatusEnum getAppMonitorStatusEnumByValue(Integer value) {
        for (AppMonitorStatusEnum item : AppMonitorStatusEnum.values()) {
            if (item.getValue().equals(value))
                return item;
        }
        return null;
    }

    private AppMonitorStatusEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

}
