package com.empire.monitoring.enums;

/**
 * 类AppTypeEnum.java的实现描述：AppHostMonitor.appType字段,应用类型
 * 应用类型:1.dbs，2.web，3.dbs&web，4.other；
 * 
 * @author arron 2018年5月21日 下午6:03:58
 */
public enum AppTypeEnum {
    /**
     * dbs
     */
    DBS("dbs", 1),
    /**
     * web
     */
    WEB("web", 2),
    /**
     * dbs&web
     */
    DBSANDWEB("dbs&web", 3),
    /**
     * other
     */
    OTHER("other", 4);
    /**
     * 应用类型名称
     */
    private String  name;
    /**
     * 应用类型code值
     */
    private Integer value;

    /**
     * 根据枚举值获得枚举对象
     * 
     * @param id
     * @return
     */
    public static AppTypeEnum getAppTypeEnumByValue(Integer value) {
        for (AppTypeEnum item : AppTypeEnum.values()) {
            if (item.getValue().equals(value))
                return item;
        }
        return null;
    }

    private AppTypeEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

}
