package com.empire.monitoring.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 类AppMainMonitor.java的实现描述：应用主表
 * 
 * @author arron 2018年5月20日 下午4:47:49
 */
@Entity
@Table(name = "t_application_main_monitor")
public class ApplicationMainMonitor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer             id;
    /**
     * 应用名称
     */
    @Column(name = "application_name")
    private String              applicationName;
    /**
     * 应用备注名称(由管理员设置)
     */
    @Column(name = "application_remark_name")
    private String              applicationRemarkName;
    /**
     * 应用备注说明(由管理员设置)
     */
    @Column(name = "application_remark_description")
    private String              applicationRemarkDescription;
    /**
     * 应用接入token(uui随机生成)
     */
    @Column(name = "application_auth_token")
    private String              applicationAuthToken;
    /**
     * 是否开启预警 true开启,false为关闭
     */
    @Column(name = "is_on_early_warning")
    private boolean             isOnEarlyWarning;
    /**
     * 应用预警类型:1.邮箱，2.微信，3.短信，；详见ApplicationWarningTypeEnum ；同时开启多种预警方式时,以分号分隔
     */
    @Column(name = "application_warning_type")
    private String              applicationWarningType;
    /**
     * 应用预警邮箱，多个邮箱以分号分隔
     */
    @Column(name = "application_early_warning_mailbox")
    private String              applicationEarlyWarningMailbox;
    /**
     * 应用预警微信号，多个手机号以分号分隔
     */
    @Column(name = "application_early_warning_wechat_number")
    private String              applicationEarlyWarningWechatNumber;
    /**
     * 应用预警手机号，多个手机号以分号分隔
     */
    @Column(name = "application_early_warning_phone")
    private String              applicationEarlyWarningPhone;

    @OneToMany(mappedBy = "applicationMainMonitor")
    private Set<AppHostMonitor> appHostMonitors = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationRemarkName() {
        return applicationRemarkName;
    }

    public void setApplicationRemarkName(String applicationRemarkName) {
        this.applicationRemarkName = applicationRemarkName;
    }

    public String getApplicationRemarkDescription() {
        return applicationRemarkDescription;
    }

    public void setApplicationRemarkDescription(String applicationRemarkDescription) {
        this.applicationRemarkDescription = applicationRemarkDescription;
    }

    public String getApplicationAuthToken() {
        return applicationAuthToken;
    }

    public void setApplicationAuthToken(String applicationAuthToken) {
        this.applicationAuthToken = applicationAuthToken;
    }

    public boolean isOnEarlyWarning() {
        return isOnEarlyWarning;
    }

    public void setOnEarlyWarning(boolean isOnEarlyWarning) {
        this.isOnEarlyWarning = isOnEarlyWarning;
    }

    public String getApplicationWarningType() {
        return applicationWarningType;
    }

    public void setApplicationWarningType(String applicationWarningType) {
        this.applicationWarningType = applicationWarningType;
    }

    public String getApplicationEarlyWarningMailbox() {
        return applicationEarlyWarningMailbox;
    }

    public void setApplicationEarlyWarningMailbox(String applicationEarlyWarningMailbox) {
        this.applicationEarlyWarningMailbox = applicationEarlyWarningMailbox;
    }

    public Set<AppHostMonitor> getAppHostMonitors() {
        return appHostMonitors;
    }

    public void setAppHostMonitors(Set<AppHostMonitor> appHostMonitors) {
        this.appHostMonitors = appHostMonitors;
    }

    public String getApplicationEarlyWarningWechatNumber() {
        return applicationEarlyWarningWechatNumber;
    }

    public void setApplicationEarlyWarningWechatNumber(String applicationEarlyWarningWechatNumber) {
        this.applicationEarlyWarningWechatNumber = applicationEarlyWarningWechatNumber;
    }

    public String getApplicationEarlyWarningPhone() {
        return applicationEarlyWarningPhone;
    }

    public void setApplicationEarlyWarningPhone(String applicationEarlyWarningPhone) {
        this.applicationEarlyWarningPhone = applicationEarlyWarningPhone;
    }

}
