package com.empire.monitoring.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 类AppHostMonitor.java的实现描述：监控的应用主机实体
 * 
 * @author arron 2018年5月20日 下午1:39:15
 */
@Entity
@Table(name = "t_app_host_monitor")
public class AppHostMonitor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer                id;
    @ManyToOne(cascade = CascadeType.PERSIST)
    //@JoinColumn:维护外键
    @JoinColumn(name = "application_main_id")
    private ApplicationMainMonitor applicationMainMonitor;
    /**
     * 监控的应用app注册名称(应用注册时提供)
     */
    @Column(name = "app_name")
    private String                 appName;
    /**
     * 应用类型:1.dbs，2.web，3.dbs&web，4.other；详见AppTypeEnum
     */
    @Column(name = "app_type")
    private Integer                appType                = 4;
    /**
     * 应用注册的监控类型:1.javamelody，2.druid，3.other；详见AppMonitorTypeEnum
     */
    @Column(name = "app_monitor_type")
    private Integer                appMonitorType         = 3;
    /**
     * 监测方式 0.不监控,1.niosocket.心跳包上报方式，2.url回调，3.主动http上报，4.mq方式，5.webservice，
     * 6.other； 推荐:mq方式/niosocket方式 ；详见AppMonitorModeEnum
     */
    @Column(name = "app_monitor_mode")
    private Integer                appMonitorMode         = 0;
    /**
     * 应用注册类型1被动(手动在监控中心添加),2.主动(app应用主动注册)；详见AppMonitorRegisterTypeEnum
     */
    @Column(name = "app_monitor_register_type")
    private Integer                appMonitorRegisterType = 1;
    /**
     * url回调监测地址(get方式)
     */
    @Column(name = "app_monitor_callback_url")
    private String                 appMonitorCallbackUrl;
    /**
     * url回调监测时间间隔-单位秒默认为:5分钟/次
     */
    @Column(name = "app_monitor_callback_time_interval")
    private Long                   appMonitorCallbackTimeInterval;
    /**
     * 最后心跳成功时间
     */
    @Column(name = "last_heartbeat_time")
    private Date                   lastHeartbeatTime;
    /**
     * 回调监测-超时时间-单位秒[用于机器可访问状态监控]默认情况:
     * 1.niosocket方式为1分钟，2.url回调方式为10分钟，3.主动http上报5分钟
     */
    @Column(name = "app_monitor_timeout")
    private Long                   appMonitorTimeout;
    /**
     * javamelody-url访问地址
     */
    @Column(name = "javamelody_url")
    private String                 javamelodyUrl;
    /**
     * druid-url访问地址
     */
    @Column(name = "druid_name")
    private String                 druid_url;
    /**
     * 监控的应用app备注名称(由管理员设置)
     */
    @Column(name = "app_remark_name")
    private String                 appRemarkName;
    /**
     * 监控的应用app备注说明(由管理员设置)
     */
    @Column(name = "app_remark_description")
    private String                 appRemarkDescription;
    /**
     * 主机名
     */
    @Column(name = "host_name")
    private String                 hostName;
    /**
     * 主机地址(ip)
     */
    @Column(name = "host_address")
    private String                 hostAddress;
    /**
     * 主机端口
     */
    @Column(name = "host_port")
    private String                 hostPort;
    /**
     * 最后一次上线时间
     */
    @Column(name = "last_online_time")
    private Date                   lastOnlineTime;
    /**
     * 当前状态:1.在线，2.离线 ；详见AppMonitorStatusEnum
     */
    @Column(name = "status")
    private Integer                status                 = 2;
    /**
     * 是否删除 true为是,false为否
     */
    @Column(name = "is_deleted")
    private boolean                isDeleted;
    /**
     * 插入时间
     */
    @Column(name = "insert_time")
    private Date                   insertTime;
    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date                   updateTime;
    /**
     * 操作ip
     */
    @Column(name = "operator_host_address")
    private String                 operatorHostAddress;
    /**
     * 操作时间
     */
    @Column(name = "operat_time")
    private Date                   operatTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Integer getAppType() {
        return appType;
    }

    public void setAppType(Integer appType) {
        this.appType = appType;
    }

    public Integer getAppMonitorType() {
        return appMonitorType;
    }

    public void setAppMonitorType(Integer appMonitorType) {
        this.appMonitorType = appMonitorType;
    }

    public Integer getAppMonitorMode() {
        return appMonitorMode;
    }

    public void setAppMonitorMode(Integer appMonitorMode) {
        this.appMonitorMode = appMonitorMode;
    }

    public Integer getAppMonitorRegisterType() {
        return appMonitorRegisterType;
    }

    public void setAppMonitorRegisterType(Integer appMonitorRegisterType) {
        this.appMonitorRegisterType = appMonitorRegisterType;
    }

    public String getAppMonitorCallbackUrl() {
        return appMonitorCallbackUrl;
    }

    public void setAppMonitorCallbackUrl(String appMonitorCallbackUrl) {
        this.appMonitorCallbackUrl = appMonitorCallbackUrl;
    }

    public Long getAppMonitorCallbackTimeInterval() {
        return appMonitorCallbackTimeInterval;
    }

    public void setAppMonitorCallbackTimeInterval(Long appMonitorCallbackTimeInterval) {
        this.appMonitorCallbackTimeInterval = appMonitorCallbackTimeInterval;
    }

    public Date getLastHeartbeatTime() {
        return lastHeartbeatTime;
    }

    public void setLastHeartbeatTime(Date lastHeartbeatTime) {
        this.lastHeartbeatTime = lastHeartbeatTime;
    }

    public Long getAppMonitorTimeout() {
        return appMonitorTimeout;
    }

    public void setAppMonitorTimeout(Long appMonitorTimeout) {
        this.appMonitorTimeout = appMonitorTimeout;
    }

    public String getJavamelodyUrl() {
        return javamelodyUrl;
    }

    public void setJavamelodyUrl(String javamelodyUrl) {
        this.javamelodyUrl = javamelodyUrl;
    }

    public String getDruid_url() {
        return druid_url;
    }

    public void setDruid_url(String druid_url) {
        this.druid_url = druid_url;
    }

    public String getAppRemarkName() {
        return appRemarkName;
    }

    public void setAppRemarkName(String appRemarkName) {
        this.appRemarkName = appRemarkName;
    }

    public String getAppRemarkDescription() {
        return appRemarkDescription;
    }

    public void setAppRemarkDescription(String appRemarkDescription) {
        this.appRemarkDescription = appRemarkDescription;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public String getHostPort() {
        return hostPort;
    }

    public void setHostPort(String hostPort) {
        this.hostPort = hostPort;
    }

    public Date getLastOnlineTime() {
        return lastOnlineTime;
    }

    public void setLastOnlineTime(Date lastOnlineTime) {
        this.lastOnlineTime = lastOnlineTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOperatorHostAddress() {
        return operatorHostAddress;
    }

    public void setOperatorHostAddress(String operatorHostAddress) {
        this.operatorHostAddress = operatorHostAddress;
    }

    public Date getOperatTime() {
        return operatTime;
    }

    public void setOperatTime(Date operatTime) {
        this.operatTime = operatTime;
    }

    public ApplicationMainMonitor getApplicationMainMonitor() {
        return applicationMainMonitor;
    }

    public void setApplicationMainMonitor(ApplicationMainMonitor applicationMainMonitor) {
        this.applicationMainMonitor = applicationMainMonitor;
    }

}
