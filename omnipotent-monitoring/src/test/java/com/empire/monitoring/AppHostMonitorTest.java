package com.empire.monitoring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.empire.monitoring.model.AppHostMonitor;
import com.empire.monitoring.model.ApplicationMainMonitor;
import com.empire.monitoring.service.AppHostMonitorService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class AppHostMonitorTest {
    @Autowired
    private AppHostMonitorService appHostMonitorService;

    /**
     * 添加测试
     */
    @Test
    public void testSave() {
        AppHostMonitor appHostMonitor = new AppHostMonitor();
        appHostMonitor.setAppName("emsite");
        appHostMonitor.setAppRemarkName("empire分布式架构");
        appHostMonitor.setAppRemarkDescription("empire团队分布式全能架构emsite");

        ApplicationMainMonitor applicationMainMonitor = new ApplicationMainMonitor();
        applicationMainMonitor.setApplicationName("建投在线客服项目");
        //建立关联
        applicationMainMonitor.getAppHostMonitors().add(appHostMonitor);
        appHostMonitor.setApplicationMainMonitor(applicationMainMonitor);
        appHostMonitorService.save(appHostMonitor);
    }
}
